//
//  DestinationAddress+CoreDataProperties.m
//  RoadyoDispatch
//
//  Created by Rahul Sharma on 20/10/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DestinationAddress+CoreDataProperties.h"

@implementation DestinationAddress (CoreDataProperties)

@dynamic desAddress;
@dynamic desAddress2;
@dynamic desLatitude;
@dynamic desLongitude;
@dynamic keyId;
@dynamic zipCode;

@end
