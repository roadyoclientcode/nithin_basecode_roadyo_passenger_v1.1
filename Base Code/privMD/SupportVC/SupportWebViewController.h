//
//  SupportWebViewController.h
//  DallasPoshPassenger
//
//  Created by Rahul Sharma on 12/05/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupportWebViewController : UIViewController<UIWebViewDelegate>

@property(nonatomic,strong) NSString *weburl;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@property(strong,nonatomic) UIButton *backButton;


@end
