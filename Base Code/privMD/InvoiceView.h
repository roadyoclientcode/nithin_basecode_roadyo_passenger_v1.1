//
//  InvoiceView.h
//  RoadyoDispatch
//
//  Created by 3Embed on 24/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AXRatingView/AXRatingView.h>

@class AXRatingView;

@interface InvoiceView : UIView
{
    UIWindow *window;
    NSDictionary *invoiceData;
    NSInteger ratingValue;
    NSString *textWritenByUser;
}
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIView *fristView;
@property (weak, nonatomic) IBOutlet UILabel *yourLastRideLabe;
@property (weak, nonatomic) IBOutlet UILabel *dayDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *needHelpBtn;
@property (weak, nonatomic) IBOutlet UILabel *line1Label;
@property (weak, nonatomic) IBOutlet UIView *secondView;
@property (weak, nonatomic) IBOutlet UILabel *redDotLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickUpWithDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickUpAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropOffWithDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropOffAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *verticalLineLabel;
@property (weak, nonatomic) IBOutlet UILabel *horizontalLineLabel;
@property (weak, nonatomic) IBOutlet UILabel *greenDotLabel;

@property (weak, nonatomic) IBOutlet UIView *thirdView;
@property (weak, nonatomic) IBOutlet UIImageView *driverImageView;
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;

@property (weak, nonatomic) IBOutlet UIButton *receiptBtn;
@property (weak, nonatomic) IBOutlet UIView *forthView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ratingViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *rateYourDriverLabel;
@property (strong, nonatomic) IBOutlet AXRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UIButton *writeCommentBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentBtnHeight;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

- (IBAction)needHelpBtnAction:(id)sender;
- (IBAction)receiptBtnAction:(id)sender;
- (IBAction)writeCommentBtnAction:(id)sender;
- (IBAction)submitBtnAction:(id)sender;


@property (strong, nonatomic) NSString *driverEmail;
@property (strong, nonatomic) NSString *apptDate;
@property (nonatomic) BOOL isComingFromBookingHistory;
-(void)showPopUpWithDetailedDict:(NSDictionary *)dict;

@end
