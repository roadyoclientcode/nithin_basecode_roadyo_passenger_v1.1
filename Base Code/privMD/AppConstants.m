//
//  AppConstants.m
//  privMD
//
//  Created by Surender Rathore on 22/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "AppConstants.h"

NSString *const kPMDPubNubPublisherKey      = @"";
NSString *const kPMDPubNubSubcriptionKey    = @"";
NSString *const kPMDGoogleMapsAPIKey        = @"";
NSString *const kPMDServerGoogleMapsAPIKey  = @"";
NSString *const kPMDFlurryId                = @"24VV3H2V2RPM6ZXG4G99";
NSString *const kPMDTestDeviceidKey         = @"C2A33350-D9CF-4A7E-8751-A36016838381";
NSString *const kPMDDeviceIdKey             = @"deviceid";
NSString *const kPMDStripeTestKey           = @"";
NSString *const kPMDStripeLiveKey           = @"";

#pragma mark - mark URLsn

#define BASE_IP  @"http://108.167.177.166/~searide/"

NSString *BASE_URL                            = BASE_IP @"services.php/";
NSString *BASE_URL_SUPPORT                    = BASE_IP;
NSString *BASE_URL_UPLOADIMAGE                = BASE_IP @"services.php/uploadImage";
NSString *const   baseUrlForXXHDPIImage       = BASE_IP @"pics/xxhdpi/";
NSString *const   baseUrlForOriginalImage     = BASE_IP @"pics/";


NSString *const termsAndCondition             = BASE_IP @"terms_of_use.php";
NSString *const privacyPolicy                 = BASE_IP @"privacy_policy.php";
NSString *const itunesURL                     = @"itunes.apple.com/";
NSString *const facebookURL                   = @"https://www.facebook.com";
NSString *const websiteLabel                  = @"''";
NSString *const websiteURL                    = @"http://";
NSString *const imgLinkForSharing             = @"";


#pragma mark - Distance Km Or Miles
double   const  kPMDDistanceMetric            = 1608.34;
NSString *const kPMDDistanceParameter         = @"Mile";
NSString *const kPMDMPHorKMPH                 = @"MPH";


// Payment Type = true then Card and cash both
// Payment Type = false then Card or cash
BOOL const kPMDPaymentType                    = true;
BOOL const kPMDCardOrCash                     = false;

#pragma mark - ServiceMethods

NSString *const kSMLiveBooking                = @"liveBooking";
NSString *const kSMGetAppointmentDetial       = @"getAppointmentDetails";
NSString *const kSMUpdateSlaveReview          = @"updateSlaveReview";
NSString *const kSMGetMasters                 = @"getMasters";
NSString *const kSMCancelAppointment          = @"cancelAppointment";
NSString *const kSMCancelOngoingAppointment   = @"cancelAppointmentRequest";

//Methods

NSString *MethodPatientSignUp                = @"slaveSignup";
NSString *MethodPatientLogin                 = @"slaveLogin";
NSString *MethodDoctorUploadImage            = @"uploadImage";
NSString *MethodPassengerLogout              = @"logout";
NSString *MethodFareCalculator               = @"fareCalculator";


//SignUp

NSString *KDASignUpFirstName                  = @"ent_first_name";
NSString *KDASignUpLastName                   = @"ent_last_name";
NSString *KDASignUpMobile                     = @"ent_mobile";
NSString *KDASignUpEmail                      = @"ent_email";
NSString *KDASignUpPassword                   = @"ent_password";
NSString *KDASignUpAddLine1                   = @"ent_address_line1";
NSString *KDASignUpAddLine2                   = @"ent_address_line2";
NSString *KDASignUpAccessToken                = @"ent_token";
NSString *KDASignUpDateTime                   = @"ent_date_time";
NSString *KDASignUpCountry                    = @"ent_country";
NSString *KDASignUpCity                       = @"ent_city";
NSString *KDASignUpLanguage                   = @"ent_lang";
NSString *KDASignUpDeviceType                 = @"ent_device_type";
NSString *KDASignUpDeviceId                   = @"ent_dev_id";
NSString *KDASignUpPushToken                  = @"ent_push_token";
NSString *KDASignUpZipCode                    = @"ent_zipcode";
NSString *KDASignUpCreditCardNo               = @"ent_cc_num";
NSString *KDASignUpCreditCardCVV              = @"ent_cc_cvv";
NSString *KDASignUpCreditCardExpiry           = @"ent_cc_exp";
NSString *KDASignUpTandC                      = @"ent_terms_cond";
NSString *KDASignUpPricing                    = @"ent_pricing_cond";
NSString *KDASignUpReferralCode               = @"ent_referral_code";
NSString *KDASignUpLatitude                   = @"ent_latitude";
NSString *KDASignUpLongitude                  = @"ent_longitude";

// Login

NSString *KDALoginEmail                       = @"ent_email";
NSString *KDALoginPassword                    = @"ent_password";
NSString *KDALoginDeviceType                  = @"ent_device_type";
NSString *KDALoginDevideId                    = @"ent_dev_id";
NSString *KDALoginPushToken                   = @"ent_push_token";
NSString *KDALoginUpDateTime                  = @"ent_date_time";


//Upload
NSString *KDAUploadDeviceId                    = @"ent_dev_id";
NSString *KDAUploadSessionToken                = @"ent_sess_token";
NSString *KDAUploadImageName                   = @"ent_snap_name";
NSString *KDAUploadImageChunck                 = @"ent_snap_chunk";
NSString *KDAUploadfrom                        = @"ent_upld_from";
NSString *KDAUploadtype                        = @"ent_snap_type";
NSString *KDAUploadDateTime                    = @"ent_date_time";
NSString *KDAUploadOffset                      = @"ent_offset";

// Logout the user

NSString *KDALogoutSessionToken                = @"user_session_token";
NSString *KDALogoutUserId                      = @"logout_user_id";



//Parsms for checking user loged out or not

NSString *KDAcheckUserId                        = @"user_id";
NSString *KDAcheckUserSessionToken              = @"ent_sess_token";
NSString *KDAgetPushToken                       = @"ent_push_token";

//Params to store the Country & City.

NSString *KDACountry                            = @"country";
NSString *KDACity                               = @"city";
NSString *KDALatitude                           = @"latitudeQR";
NSString *KDALongitude                          = @"longitudeQR";

//params for Firstname
NSString *KDAFirstName                          = @"ent_first_name";
NSString *KDALastName                           = @"ent_last_name";
NSString *KDAEmail                              = @"ent_email";
NSString *KDAPhoneNo                            = @"ent_mobile";
NSString *KDAPassword                           = @"ent_password";


#pragma mark - NSUserDeafults Keys

NSString *const KNSUPubnubPublishKey                  = @"pubnubPublish";
NSString *const kNSUPubnubSubscribeKey                = @"pubnubSubscribe";

NSString *const kNSUUserPubnubChannel                 = @"userChannel";
NSString *const kNSUServerPubnubChannel               = @"serverChhannel";
NSString *const kNSUPresensePubnubChannel             = @"presenseChannel";
NSString *const kNSUDriverPubnubChannel               = @"driverChannel";

NSString *const kNSUUserEmailID                       = @"userEmailId";
NSString *const kNSUUserReferralCode                  = @"referralCode";
NSString *const kNSUUserShareMessage                  = @"shareMessage";


NSString *const kNSUPlaceAPIKey                       = @"placeAPIKey";
NSString *const kNSUStripeKey                         = @"stripeKey";

NSString *const kNSUMongoDataBaseAPIKey               = @"mongoDBapi";
NSString *const kNSUIsPassengerBookedKey              = @"passengerBooked";
NSString *const kNSUPassengerBookingStatusKey         = @"STATUSKEY";
NSString *const KUBERCarArrayKey                      = @"carTypeArray";
NSString *const kNSUPatientCouponkey                  = @"coupon";
NSString *const kNSUPatientPaypalkey                  = @"paypal";

#pragma mark - PushNotification Payload Keys

 NSString *const kPNPayloadDoctorNameKey            = @"n";
 NSString *const kPNPayloadAppoinmentTimeKey        = @"dt";
 NSString *const kPNPayloadDistanceKey              = @"dis";
 NSString *const kPNPayloadEstimatedTimeKey         = @"eta";
 NSString *const kPNPayloadDoctorEmailKey           = @"e";
 NSString *const kPNPayloadDoctorContactNumberKey   = @"ph";
 NSString *const kPNPayloadProfilePictureUrlKey     = @"pic";
NSString *const  kPNPayloadStatusIDKey              = @"nt";
NSString *const  kPNPayloadAppointmentIDKey         = @"id";

#pragma mark - Car DescriptionKeys
NSString *const KUBERCarTypeID               = @"type_id";
NSString *const KUBERCarTypeName             = @"type_name";
NSString *const KUBERCarMaxSize              = @"max_size";
NSString *const KUBERCarBaseFare             = @"basefare";
NSString *const KUBERCarMinFare              = @"min_fare";
NSString *const KUBERCarPricePerMin          = @"price_per_min";
NSString *const KUBERCarPricePerKM           = @"price_per_km";
NSString *const KUBERCarTypeDescription      = @"type_desc";
NSString *const KUBERDriverStatus            = @"status";


#pragma mark - Notification Name keys
NSString *const kNotificationNewCardAddedNameKey   = @"cardAdded";
NSString *const kNotificationCardDeletedNameKey   = @"cardDeleted";
NSString *const kNotificationLocationServicesChangedNameKey = @"CLChanged";
NSString *const kNotificationBookingConfirmationNameKey = @"bookingConfirmed";

#pragma mark - Network Error
NSString *const kNetworkErrormessage          = @"No network connection";

NSString *const KNUCurrentLat          = @"latitude";
NSString *const KNUCurrentLong         = @"longitude";
NSString *const KNUserCurrentCity      = @"usercity";
NSString *const KNUserCurrentState     = @"userstate";
NSString *const KNUserCurrentCountry   = @"userCountry";

NSString *const KUDriverEmail           = @"DriverEmail";
NSString *const KUBookingDate           = @"BookingDate";
NSString *const KUBookingID             = @"BookingID";
