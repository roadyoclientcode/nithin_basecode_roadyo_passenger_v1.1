//
//  CancelView.m
//  RoadyoDispatch
//
//  Created by 3Embed on 19/07/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "CancelView.h"
#import "XDKAirMenuController.h"
#import "PatientViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"

@implementation CancelView
@synthesize onCompletion;
@synthesize yourLastRideLabe, dayDateLabel, closeBtn;
@synthesize pickUpWithDateLabel, pickUpAddressLabel, dropOffWithDateLabel, dropOffAddressLabel;
@synthesize greenDotLabel, redDotLabel, verticalLineLabel, horizontalLineLabel;
@synthesize driverImageView, driverNameLabel, totalAmountLabel;
@synthesize cancelBtn;

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"CancelView" owner:self options:nil] firstObject];
    return self;
}



- (IBAction)closeBtnAction:(id)sender
{
    onCompletion(0);
    [self hidePOPup];
}

- (IBAction)cancelBtnAction:(id)sender
{
    [self sendRequestForCancelAppointment:appoinmentDetails];
}

-(void)showPopUpWithDetailedDict:(NSDictionary *)dict
{
    window = [[UIApplication sharedApplication] keyWindow];
    self.frame = window.frame;
    [window addSubview:self];
    [self layoutIfNeeded];
    self.contentView.alpha = 0.3;
    
    self.contentView.layer.cornerRadius = 8.0f;
    self.contentView.layer.masksToBounds = YES;
    
    greenDotLabel.layer.cornerRadius = greenDotLabel.frame.size.width/2;
    greenDotLabel.layer.masksToBounds = YES;
    
    redDotLabel.layer.cornerRadius = redDotLabel.frame.size.width/2;
    redDotLabel.layer.masksToBounds = YES;
    
    self.cancelBtn.layer.cornerRadius = 5.0f;
    self.cancelBtn.layer.masksToBounds = YES;
    
    appoinmentDetails = [dict mutableCopy];
    [self updateUI];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                         
                     }];
}

-(void)hidePOPup
{
    self.contentView.alpha = 1;
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.alpha = 0.5;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}

/**
 *  cancel live booking
 */
-(void)sendRequestForCancelAppointment:(NSDictionary *)dictionary
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:window withMessage:NSLocalizedString(@"Cancelling...", @"Cancelling...")];
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *driverEmail = flStrForStr(dictionary[@"email"]);
    NSString *appointmntDate = flStrForStr(dictionary[@"apntDt"]);
    NSString *bid = flStrForStr(dictionary[@"bid"]);
    
    NSString *currentDate = [Helper getCurrentDateTime];
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_dri_email":flStrForStr(driverEmail),
                             @"ent_appnt_dt":flStrForStr(appointmntDate),
                             @"ent_bid":flStrForStr(bid),
                             @"ent_date_time":currentDate,
                             };
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMCancelAppointment
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       //handle success response
                                       [self parseCancelAppointmentResponse:response];
                                   }
                               }];
}

-(void)parseCancelAppointmentResponse:(NSDictionary*)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    if (response == nil)
    {
        return;
    }
    else if([response[@"errFlag"] integerValue] == 0 && ([response[@"errNum"] integerValue] == 42 || [response[@"errNum"] integerValue] == 43))
    {
        onCompletion(1);
        [self hidePOPup];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
    }
    else
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
    }
}

-(void)updateUI
{
    [Helper setToLabel:yourLastRideLabe Text:NSLocalizedString(@"YOUR RIDE DETAILS", @"YOUR RIDE DETAILS") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x333333)];
    
    NSString *rideDate = [self getDateAndTime:flStrForObj(appoinmentDetails[@"apntDt"])];
    [Helper setToLabel:dayDateLabel Text:rideDate WithFont:Lato_Regular FSize:11 Color:UIColorFromRGB(0x919191)];
    
    NSString *pickUp = NSLocalizedString(@"PICK UP", @"PICK UP");
//    NSString *pTime = [self getTime:flStrForObj(appoinmentDetails[@"pickupDt"])];
    [Helper setToLabel:pickUpWithDateLabel Text:[NSString stringWithFormat:@"%@", pickUp] WithFont:Lato_Regular FSize:11 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:pickUpAddressLabel Text:flStrForObj(appoinmentDetails[@"addrLine1"]) WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x6f6f6f)];
    
    NSString *dropOff = NSLocalizedString(@"DROP OFF", @"DROP OFF");
//    NSString *dTime = [self getTime:flStrForObj(appoinmentDetails[@"dropDt"])];
    [Helper setToLabel:dropOffWithDateLabel Text:[NSString stringWithFormat:@"%@", dropOff] WithFont:Lato_Regular FSize:11 Color:UIColorFromRGB(0x333333)];
    
    NSString *dropAddr = flStrForObj(appoinmentDetails[@"dropLine1"]);
    if (!dropAddr.length)
    {
        dropAddr = NSLocalizedString(@"Drop off address not added.", @"Drop off address not added.");
    }
    [Helper setToLabel:dropOffAddressLabel Text:dropAddr WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x6f6f6f)];
    
    driverImageView.layer.cornerRadius = driverImageView.frame.size.width/2;
    driverImageView.layer.masksToBounds = YES;
    
    NSString *strImageURL = flStrForStr(appoinmentDetails[@"pPic"]);
    if (strImageURL.length)
    {
        strImageURL = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,appoinmentDetails[@"pPic"]];
        [driverImageView sd_setImageWithURL:[NSURL URLWithString:strImageURL]
                           placeholderImage:[UIImage imageNamed:@"driverImage"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                      
                                      if (error || !image)
                                      {
                                          driverImageView.image = [UIImage imageNamed:@"driverImage"];
                                      }
                                      else
                                      {
                                          driverImageView.image = image;
                                      }
                                  }];
    }
    else
    {
        driverImageView.image = [UIImage imageNamed:@"driverImage"];
    }
    
    [Helper setToLabel:driverNameLabel Text:[[NSString stringWithFormat:@"%@",flStrForObj(appoinmentDetails[@"fname"])] uppercaseString] WithFont:Lato_Regular FSize:15 Color:UIColorFromRGB(0x707070)];
    
    NSString *totalAmount =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(appoinmentDetails[@"amount"]) floatValue]];
    [Helper setToLabel:totalAmountLabel Text:totalAmount WithFont:Lato_Regular FSize:15 Color:UIColorFromRGB(0x333333)];
    [Helper setButton:cancelBtn Text:NSLocalizedString(@"CANCEL BOOKING", @"CANCEL BOOKING") WithFont:Lato_Regular FSize:14 TitleColor:UIColorFromRGB(0xFFFFFF) ShadowColor:nil];
}


-(NSString*)getTime:(NSString *)aDatefromServer
{
    NSString *mGetting = [NSString stringWithFormat:@"%@",aDatefromServer];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date  = [dateFormatter dateFromString:mGetting];
    dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *retTime = [dateFormatter stringFromDate:date];
    return retTime;
}

-(NSString*)getDateAndTime:(NSString *)aDatefromServer{
    
    NSString *mGetting = [NSString stringWithFormat:@"%@",aDatefromServer];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date  = [dateFormatter dateFromString:mGetting];
    dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString *retTime = [dateFormatter stringFromDate:date];
    return retTime;
}

@end
