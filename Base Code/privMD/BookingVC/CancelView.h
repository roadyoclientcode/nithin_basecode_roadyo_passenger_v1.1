//
//  CancelView.h
//  RoadyoDispatch
//
//  Created by 3Embed on 19/07/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CancelView : UIView
{
    UIWindow *window;
    NSDictionary *appoinmentDetails;
}
@property (nonatomic, copy)   void (^onCompletion)(NSInteger isCancel);


@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIView *fristView;
@property (weak, nonatomic) IBOutlet UILabel *yourLastRideLabe;
@property (weak, nonatomic) IBOutlet UILabel *dayDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UILabel *line1Label;
@property (weak, nonatomic) IBOutlet UIView *secondView;
@property (weak, nonatomic) IBOutlet UILabel *redDotLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickUpWithDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickUpAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropOffWithDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropOffAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *verticalLineLabel;
@property (weak, nonatomic) IBOutlet UILabel *horizontalLineLabel;
@property (weak, nonatomic) IBOutlet UILabel *greenDotLabel;

@property (weak, nonatomic) IBOutlet UIView *thirdView;
@property (weak, nonatomic) IBOutlet UIImageView *driverImageView;
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

- (IBAction)closeBtnAction:(id)sender;
- (IBAction)cancelBtnAction:(id)sender;


//@property (strong, nonatomic) NSString *driverEmail;
//@property (strong, nonatomic) NSString *apptDate;
//@property (strong, nonatomic) NSString *pickUpAddress;
//@property (strong, nonatomic) NSString *dropUpAddress;
//@property (strong, nonatomic) NSString *driverName;
//@property (strong, nonatomic) NSString *amount;
//@property (strong, nonatomic) NSString *bookingID;

-(void)showPopUpWithDetailedDict:(NSDictionary *)dict;

@end
