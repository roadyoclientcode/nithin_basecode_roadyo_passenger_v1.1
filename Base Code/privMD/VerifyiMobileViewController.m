//
//  VerifyiMobileViewController.m
//  ParadiseRide
//
//  Created by Rahul Sharma on 27/03/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "VerifyiMobileViewController.h"
#import "CardLoginViewController.h"
#import "ViewController.h"

@interface VerifyiMobileViewController ()

@end

@implementation VerifyiMobileViewController


-(void) createNavLeftButton
{
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImage.size.width,buttonImage.size.height)];
    [Helper setButton:navCancelButton Text:NSLocalizedString(@"BACK", @"BACK") WithFont:Roboto_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateSelected];
    [navCancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navCancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    navCancelButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:11];
    [navCancelButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}


-(void)createNavRightButton
{
    UIButton *navNextButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    [navNextButton setFrame:CGRectMake(0,0,buttonImage.size.width,buttonImage.size.height)];
    
    [navNextButton addTarget:self action:@selector(NextButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    [Helper setButton:navNextButton Text:NSLocalizedString(@"NEXT", @"NEXT") WithFont:Roboto_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navNextButton setTitle:NSLocalizedString(@"NEXT", @"NEXT") forState:UIControlStateSelected];
    [navNextButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navNextButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    [navNextButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

-(void)cancelButtonClicked {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)NextButtonClicked {
    
    if (_verifyimobileTextField.text.length == 0) {
        
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter the verification code.", @"Please enter the verification code.")];
    }
    else {
        
        [self validateUserMobileNumber];
        
    }
    

}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"CardController"])
    {
        CardLoginViewController *CLVC = (CardLoginViewController*)[segue destinationViewController];
        CLVC.isComingFromPayment = 3;
    }
}

- (void) validateUserMobileNumber
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Validating Number..", @"Validating Number..")];
    
    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    NSString *mobileNo =  _myNumber;
    NSDictionary *params = @{
                             @"ent_dev_id":deviceId,
                             @"ent_phone":mobileNo,
                             @"ent_code":_verifyimobileTextField.text
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"verifyPhone"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       if (!response)
                                       {
                                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:NSLocalizedString(@"Message", @"Message")] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
                                           [alertView show];
                                           self.verifyimobileTextField.text = @"";
                                           [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                       }
                                       else if ([response objectForKey:@"Error"])
                                       {
                                           self.verifyimobileTextField.text = @"";
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"Error"]];
                                           [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                       }
                                       else if ([response[@"errFlag"]integerValue] == 0)
                                       {
                                           [self sendServiceToSignedUpUser];
                                       }
                                       else if ([response[@"errFlag"]integerValue] == 1)
                                       {
                                           self.verifyimobileTextField.text = @"";
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"errMsg"]];
                                           [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                       }
                                   }
                               }];
}


-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg-568h"]];
    self.navigationController.navigationBarHidden=NO;
    self.navigationItem.hidesBackButton = NO;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    [self createNavLeftButton];
    [self createNavRightButton];
    
    NSString *msg = NSLocalizedString(@"We have sent a verification code on your number ", @"We have sent a verification code on your number ");
    msg = [msg stringByAppendingString:_myNumber];
    self.numberMessageLabel.text = msg;
    
    [self createNavView];
    [self.verifyimobileTextField becomeFirstResponder];
}

- (void) viewWillAppear:(BOOL)animated
{
    self.navigationItem.hidesBackButton = YES;
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.navigationItem.titleView  = nil;
}

-(void)createNavView
{
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(80,-10, 160, 50)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(0,0, 147, 30)];
    navTitle.text = NSLocalizedString(@"CREATE ACCOUNT", @"CREATE ACCOUNT");
    navTitle.textColor = UIColorFromRGB(0xffffff);
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.font = [UIFont fontWithName:Roboto_Light size:13];
    [navView addSubview:navTitle];
    UIImageView *navImage = [[UIImageView alloc]initWithFrame:CGRectMake(0,30,147,7)];
    navImage.image = [UIImage imageNamed:@"signup_timer_second"];
    [navView addSubview:navImage];
    self.navigationItem.titleView = navView;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getVerificationCode
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Please wait...", @"Please wait...")];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *params = @{
                             @"ent_dev_id":deviceID,
                             @"ent_mobile":_myNumber,
                             KDASignUpDateTime:[Helper getCurrentDateTime],
                            };
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getVerificationCode"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       [self validateVerificationCodeResponse:response];
                                   }
                               }];
}

-(void)validateVerificationCodeResponse :(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"Error"]];
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            
        }
        else if ([dictResponse[@"errFlag"] intValue] == 1 && [dictResponse[@"errNum"] intValue] == 113)
        {
             [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"errMsg"]];
        }
        else if ([dictResponse[@"errFlag"] intValue] == 1 && [dictResponse[@"errNum"] intValue] == 108)
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response [@"test"][@"response"][@"error"]];
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"errMsg"]];
        }
    }
}



- (IBAction)callmeButtonClicked:(id)sender
{
}


- (IBAction)sendmemeButtonClicked:(id)sender
{
    [self getVerificationCode];
}

#pragma mark - TextFields
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
   
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.text.length == 5) {
        
        [self validateUserMobileNumber];
    }
}


- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    return YES;
}

- (IBAction)textFieldValueChanged:(UITextField *)sender {
   
    if(sender.text.length == 5) {
        [self.view endEditing:YES];
    }
    
}



//SignupServiceCall

-(void)sendServiceToSignedUpUser
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:NSLocalizedString(@"Registering...", @"Registering...")];
    
    NSString *pToken = @"";
    if([[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken])
    {
        pToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken];
    }
    else
    {
        pToken = @"";
    }
    
    NSString *country = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCountry])
        country = @"";
    else
        country = [[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCountry];
    
    
    NSString *city = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCity])
        city = @"";
    else
        city = [[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCity];
    
    
    NSString *lat = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat])
        lat = @"";
    else
        lat = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat];
    
    NSString *lon = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong])
        lon = @"";
    else
        lon = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong];
    
    
    
    NSString *deviceId;
    if (IS_SIMULATOR) {
        
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *params = @{KDASignUpFirstName:[_getSignupDetails objectAtIndex:0],
                             KDASignUpLastName:[_getSignupDetails objectAtIndex:1],
                             KDASignUpEmail:[_getSignupDetails objectAtIndex:2],
                             KDASignUpMobile:[_getSignupDetails objectAtIndex:3],
                             KDASignUpPassword:[_getSignupDetails objectAtIndex:4],
                             KDASignUpReferralCode:[_getSignupDetails objectAtIndex:5],
                             KDASignUpDeviceId:deviceId,
                             KDASignUpAccessToken:@"",
                             KDASignUpCity:city,
                             KDASignUpCountry:country,
                             KDASignUpPushToken:pToken,
                             KDASignUpLatitude:lat,
                             KDASignUpLongitude:lon,
                             KDASignUpTandC:@"1",
                             KDASignUpPricing:@"1",
                             KDASignUpDeviceType:@"1",                             KDASignUpLanguage:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedLanguage"],
                             KDASignUpDateTime:[Helper getCurrentDateTime],
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:MethodPatientSignUp
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       [self signupResponse:response];
                                   }
                               }];
    [[NSUserDefaults standardUserDefaults] setObject:[_getSignupDetails objectAtIndex:0] forKey:KDAFirstName];
    [[NSUserDefaults standardUserDefaults] setObject:[_getSignupDetails objectAtIndex:1] forKey:KDALastName];
    [[NSUserDefaults standardUserDefaults] setObject:[_getSignupDetails objectAtIndex:2] forKey:KDAEmail];
    [[NSUserDefaults standardUserDefaults] setObject:[_getSignupDetails objectAtIndex:3] forKey:KDAPhoneNo];
    [[NSUserDefaults standardUserDefaults] setObject:[_getSignupDetails objectAtIndex:4] forKey:KDAPassword];
    [[NSUserDefaults standardUserDefaults]synchronize];
}


-(void)signupResponse:(NSDictionary*)response
{
    if(response == nil)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        return;
    }
    if (response[@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:response[@"Error"]];
    }
    else
    {
        NSDictionary *itemList = [[NSDictionary alloc]init];
        itemList = [response mutableCopy];
        if ([itemList[@"errFlag"] intValue] == 1)
        {
            ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
            [progressIndicator hideProgressIndicator];
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:itemList[@"errMsg"]];
        }
        else
        {
            NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
            [ud setObject:flStrForStr(itemList[@"token"]) forKey:KDAcheckUserSessionToken];
            [ud setObject:flStrForStr(itemList[@"apiKey"]) forKey:kNSUMongoDataBaseAPIKey];
            
            [ud setObject:flStrForStr(itemList[@"pub"]) forKey:KNSUPubnubPublishKey];
            [ud setObject:flStrForStr(itemList[@"sub"]) forKey:kNSUPubnubSubscribeKey];

            [ud setObject:flStrForStr(itemList[@"chn"]) forKey:kNSUUserPubnubChannel];
            [ud setObject:flStrForStr(itemList[@"serverChn"]) forKey:kNSUServerPubnubChannel];
            [ud setObject:flStrForStr(itemList[@"presenseChn"]) forKey:kNSUPresensePubnubChannel];
            
            [ud setObject:flStrForStr(itemList[@"ClientmapKey"]) forKey:kNSUPlaceAPIKey];
            [ud setObject:flStrForStr(itemList[@"stipeKey"]) forKey:kNSUStripeKey];

            [ud setObject:flStrForStr(itemList[@"email"]) forKey:kNSUUserEmailID];
            [ud setObject:flStrForStr(itemList[@"coupon"]) forKey:kNSUUserReferralCode];
            [ud setObject:flStrForStr(itemList[@"shareMessage"]) forKey:kNSUUserShareMessage];
            
            [ud synchronize];
            
            NSMutableArray *carTypes = [[NSMutableArray alloc]initWithArray:itemList[@"types"]];
            if (!carTypes || !carTypes.count)
            {
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:carTypes forKey:KUBERCarArrayKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            if(![ud objectForKey:KDAcheckUserSessionToken])
            {
                return;
            }
            
            if(_pickedImage)
            {
                UploadFile * upload = [[UploadFile alloc]init];
                upload.delegate = self;
                [upload uploadImageFile:_pickedImage];
            }
            else
            {
                ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
                [progressIndicator hideProgressIndicator];
                [self performSegueWithIdentifier:@"CardController" sender:self];
            }
        }
        
    }
}

#pragma UploadFileDelegate

-(void)uploadFile:(UploadFile *)uploadfile didUploadSuccessfullyWithUrl:(NSArray *)imageUrls
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
   [self performSegueWithIdentifier:@"CardController" sender:self];
}

#pragma UploadFileDelegate
-(void)uploadFile:(UploadFile *)uploadfile didFailedWithError:(NSError *)error{
    
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    [self performSegueWithIdentifier:@"CardController" sender:self];
    
    [Helper showAlertWithTitle:NSLocalizedString(@"Oops!", @"Oops!") Message:NSLocalizedString(@"Your profile photo has not been updated try again.", @"Your profile photo has not been updated try again.")];
}

@end
