//
//  MessageView.m
//  RoadyoDispatch
//
//  Created by 3Embed on 26/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "MessageView.h"

@implementation MessageView
@synthesize textViewPlaceHolder;
@synthesize onCompletion;
@synthesize headingText;
@synthesize isDispute;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"MessageView" owner:self options:nil] firstObject];
    return self;
}

-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window;
{
    self.frame = window.frame;
    [window addSubview:self];
    [self layoutIfNeeded];
    self.contentView.alpha = 0.3;
    
    self.contentView.layer.cornerRadius = 8.0f;
    self.contentView.layer.masksToBounds = YES;
    
    self.submitBtn.layer.cornerRadius = 4.0f;
    self.submitBtn.layer.masksToBounds = YES;

    [Helper setToLabel:self.headingLabel Text:headingText WithFont:Roboto_Regular FSize:15 Color:[UIColor blackColor]];
    
    self.textView.textColor = [UIColor lightGrayColor];
    self.textView.delegate = self;
    self.textView.text = textViewPlaceHolder;
    self.textView.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.2];
    [self handleKeyboard];

    [UIView animateWithDuration:0.5
                     animations:^{
                                                  
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                     }];
}

-(void)hidePOPup
{
    self.contentView.alpha = 1;
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.alpha = 0.1;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}


- (IBAction)submitBtnAction:(id)sender
{
    NSString *message;
    [self removeKeyboardNotofication];
    if ([self.textView.text isEqualToString:textViewPlaceHolder])
    {
        message = @"";
    }
    else
    {
        message = self.textView.text;
    }
    if (isDispute)
    {
        [self sendRequestForDisputeSubmit:message];
    }
    else
    {
        onCompletion(1, message);
        [self hidePOPup];
    }
}

- (IBAction)cancelBtnAction:(id)sender
{
    onCompletion(2, @"");
    [self removeKeyboardNotofication];
    [self hidePOPup];
}


- (void)removeKeyboardNotofication
{
    [self.textView resignFirstResponder];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)sendRequestForDisputeSubmit:(NSString *)disputeMsgText
{    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self withMessage:NSLocalizedString(@"Disputing...", @"Disputing...")];
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    
    NSString *currentDate = [Helper getCurrentDateTime];
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_appnt_dt":self.appoinmentDate,
                             @"ent_date_time":currentDate,
                             @"ent_report_msg":disputeMsgText,
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"reportDispute"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       [self parseDisputeResponse:response];
                                   }
                                   else
                                   {
                                       ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                                       [pi hideProgressIndicator];
                                   }
                               }];
    
    
}

-(void)parseDisputeResponse:(NSDictionary*)response{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    if (response == nil)
    {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:NSLocalizedString(@"Error", @"Error")]];
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            [self clearUserDefaultsAfterBookingCompletion];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBCOMPLETED" object:nil userInfo:nil];
            [self hidePOPup];
        }
        else
        {
            [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"errMsg"]];
        }
    }
}

-(void)clearUserDefaultsAfterBookingCompletion
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud removeObjectForKey:kNSUPassengerBookingStatusKey];
    [ud removeObjectForKey:kNSUDriverPubnubChannel];
    [ud setBool:NO forKey:kNSUIsPassengerBookedKey];
    [ud setBool:NO forKey:@"isServiceCalledOnce"];
    [ud removeObjectForKey:@"MODEL"];
    [ud removeObjectForKey:@"selectedindexforcarcolor"];
    [ud removeObjectForKey:@"PLATE"];
    [ud synchronize];
}

#pragma mark -
#pragma mark TextView Delegate methods

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView.textColor == [UIColor lightGrayColor])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];

    }
    return YES;
}


-(void)textViewDidChange:(UITextView *)textView
{
    if(textView.text.length == 0)
    {
        textView.textColor = [UIColor lightGrayColor];
        textView.text = textViewPlaceHolder;
        [textView resignFirstResponder];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView hasText])
    {
        textView.text = textView.text;
    }
    else
    {
        [textView setTextColor:[UIColor lightGrayColor]];
        textView.text = textViewPlaceHolder;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        if(textView.text.length == 0)
        {
            textView.text = @"";
        }
        [textView resignFirstResponder];
        return NO;
    }
    // For any other character return TRUE so that the text gets added to the view
    return YES;
}

/**
 *  Handle Keyboard Show & Hide
 */
- (void)handleKeyboard
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

/**
 *  Keyboard Shown
 */
- (void)keyboardWillShown:(NSNotification *)notification
{
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    float height = MIN(keyboardSize.height,keyboardSize.width);
    [self moveViewUpWithKeyboardHeight:height];
}

/**
 *  Keyboard will be hidden
 */
- (void)keyboardWillBeHidden:(NSNotification *)notification
{
    [self moveViewDown];
}

- (void)moveViewUpWithKeyboardHeight:(float)height
{
    float heightOfTextfield = CGRectGetMaxY(self.textView.frame) + CGRectGetMinY([self.textView superview].frame) + self.contentView.frame.origin.y + height;
    
    float windowHeight = self.frame.size.height;
    
    if ((heightOfTextfield - windowHeight) > 0) {
        
        [UIView animateWithDuration:0.4
                         animations:^{
                             
                             CGRect frame = self.contentView.frame;
                             frame.origin.y -= (heightOfTextfield - windowHeight);
                             self.contentView.frame = frame;
                         }];
    }
}
- (void)moveViewDown
{
    [UIView animateWithDuration:0.4
                     animations:^{
                         
                         self.contentView.center = [self.contentView superview].center;
                     }];
}

@end
